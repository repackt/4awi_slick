package slick_introduction;

import org.newdawn.slick.Graphics;

public class Circle {

	private int x, y;
	private int width, height;
	private int angle;
	private int movement;

	// private MoveStrategy mls;

	public Circle(int x, int y, int width, int height, int angle) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.angle = angle;

	}

	public void update(int delta) {
		if (movement == 0) {
			this.x++;
			if (x >= 650) {
				movement = 1;
			}
		}
		if (movement == 1) {
			this.x--;
			if (x <= 0) {
				movement = 0;
			}
		}
	}

	public void render(Graphics g) {
		g.drawRoundRect(this.x, this.y, this.width, this.height, this.angle);
	}
}
