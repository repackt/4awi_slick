package slick_introduction;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Main extends BasicGame {

	// private Image backgroundImage;
	private Rectangle rectangle;
	private Oval Ovi;
	private Circle Circ;
	private SpaceShip ship1;
	private boolean flyingShot = false;
	private Circle shot;
	List<Snowflake> snowflakes;
	List<Actor> actors;

	public Main() {
		super("HelloWorld_Game");
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		// this.backgroundImage.draw();
		rectangle.render(g);
		Ovi.render(g);
		Circ.render(g);
		ship1.render(g);
		shot.render(g);

		for (Actor actor : actors) {
			actor.render(g);
		}
		for (int i = 0; i < snowflakes.size(); i++) {
			snowflakes.get(i).render(g);
		}
	}

//	@Override
//	public void keyPressed(int key, char c) {
//		// TODO Auto-generated method stub
//		super.keyPressed(key, c);
//	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		this.rectangle = new Rectangle(50, 30, 120, 170);
		this.Ovi = new Oval(500, 200, 100, 350);
		this.Circ = new Circle(200, 200, 150, 150, 90);
		this.ship1 = new SpaceShip(320, 450);
		snowflakes = new ArrayList<Snowflake>();
		actors = new ArrayList<Actor>();
		this.shot = new Circle(50, 50, 50, 50, 90);
//
//		for (int i = 0; i < 90; i++) {
//			snowflakes.add(new Snowflake(Snowflake.size.small));
//			snowflakes.add(new Snowflake(Snowflake.size.medium));
//			snowflakes.add(new Snowflake(Snowflake.size.big));
//		}
	}

	@Override
	public void update(GameContainer arg0, int delta) throws SlickException {
		rectangle.update(delta);
		Ovi.update(delta);
		Circ.update(delta);
		ship1.update(arg0, delta);

		for (int i = 0; i < snowflakes.size(); i++) {
			snowflakes.get(i).update(delta);
		}

		for (int i = 0; i < actors.size(); i++) {
			actors.get(i).update(delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
