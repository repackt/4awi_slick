package slick_introduction;

import org.lwjgl.util.Timer;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class SpaceShip {
	private double x, y;
	private int width, height;
	private boolean moveLeft = false;
	private boolean moveRight = false;
	private Animation animation;

	public SpaceShip(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.width = 150;
		this.height = 90;

		animation = new Animation();
		Image i1;
		try {
			for (int i = 1; i < 10; i++) {
				String pre;
				if (i < 10) {
					pre = "0";
				} else {
					pre = "";
				}
				i1 = new Image("testdata/winterworld/smallfighter00" + pre + i + ".png");
				animation.addFrame(i1, 100);
			}

			for (int i = 10; i > 0; i--) {
				String pre;
				if (i < 10) {
					pre = "0";
				} else {
					pre = "";
				}
				i1 = new Image("testdata/winterworld/smallfighter00" + pre + i + ".png");
				animation.addFrame(i1, 100);
			}

		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void update(GameContainer arg0, int delta) {
		moveLeft = arg0.getInput().isKeyDown(Input.KEY_LEFT);
		moveRight = arg0.getInput().isKeyDown(Input.KEY_RIGHT);

		if (x >= -15) {

			if (moveLeft == true) {
				this.x--;
			}
			if (moveRight == true) {
				this.x++;
			}
		}

		if (x < -15) {
			x++;
		}
		if (x > 720) {
			x--;
		}
	}

	public void render(Graphics g) {
		// g.setColor(Color.red);
		// g.fillOval((int) this.x, (int) this.y, this.width, this.height);
		animation.draw((int) this.x, (int) this.y);
		// g.setColor(Color.white);
	}
}
