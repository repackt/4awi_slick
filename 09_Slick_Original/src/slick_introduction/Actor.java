package slick_introduction;

import org.newdawn.slick.Graphics;

public interface Actor {
	public void update(int delta);

	public void render(Graphics g);
}
