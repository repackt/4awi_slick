package slick_introduction;

import org.newdawn.slick.Graphics;

public class Oval {

	private int x, y;
	private int width, height;

	public Oval(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {
		this.y++;
		if (y >= 600) {
			y = -360;
		}
	}

	public void render(Graphics g) {
		g.drawOval(this.x, this.y, this.width, this.height);
	}
}