package slick_introduction;

import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake {
	private int x, y, width, height;
	private size size;
	private double velocity;

	Random rnd = new Random();

	public enum size {
		small, medium, big;
	}

	public Snowflake(size size) {
		super();
		this.x = rnd.nextInt(800);
		this.y = rnd.nextInt(900) - 900;
		this.size = size;
	}

	public void update(int delta) {
		this.y += delta * this.velocity;
	}

	public void render(Graphics g) {
		if (this.size == size.small) {
			this.width = 10;
			this.height = 10;
			this.velocity = 0.45;
		}

		if (this.size == size.medium) {
			this.width = 25;
			this.height = 25;
			this.velocity = 0.55;
		}

		if (this.size == size.big) {
			this.width = 40;
			this.height = 40;
			this.velocity = 0.65;
		}

		if (this.y > 600) {
			this.y = -50;
		}

		g.fillRoundRect(this.x, this.y, this.width, this.height, 90);
	}
}
