package slick_introduction;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ShootingStar {
	private double x, y;
	private int width, height;
	private double speedX, speedY;

	Random rnd = new Random();

	public ShootingStar(int width, int height) {
		super();
		this.x = rnd.nextInt(100) + 800;
		this.y = rnd.nextInt(100);
		this.width = width;
		this.height = height;
		this.speedX = 0.05;
		this.speedY = 0.05;
	}

	int deltatime = 0;

	public void update(int delta) {
		this.speedX += 0.007;
		this.x -= delta * this.speedX;
		this.speedY += ((Math.exp(0.002) - 1));
		this.y += delta * this.speedY;

		deltatime += delta;
		if (deltatime >= 300) {
			this.height -= 1;
			this.width -= 1;

			deltatime = 0;
		}

		if (this.x <= -3000) {
			this.x = rnd.nextInt(100) + 800;
			this.y = rnd.nextInt(100);
			this.width = 100;
			this.height = 100;
			this.speedX = 0.05;
			this.speedY = 0.05;
		}
	}

	public void render(Graphics g) {
		g.setColor(Color.yellow);
		g.fillRoundRect((int) this.x, (int) this.y, this.width, this.height, 90);
		g.setColor(Color.white);
	}
}
