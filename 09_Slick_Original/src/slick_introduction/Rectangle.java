package slick_introduction;

import org.newdawn.slick.Graphics;

public class Rectangle {
	private int x, y;
	private int width, height;
	private int angle;

	public Rectangle(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {

		this.angle++;
		this.x = (int) (Math.cos(this.angle * Math.PI / 180) * 180 + 500);
		this.y = (int) (Math.sin(this.angle * Math.PI / 180) * 180 + 300);
	}

	public void render(Graphics g) {
		g.drawRect(this.x, this.y, this.width, this.height);
	}

}
