package at.leon.games.cofferun.actors;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.Random;

public class Cloud {

	private float x, y, width, height;
	private size size;
	private float velocity;
	private Image cloud;

	Random rnd = new Random();

	public enum size {
		small, medium, big;
	}

	public Cloud(size size) throws SlickException {
		super();
		this.x = rnd.nextInt(2000);
		this.y = rnd.nextInt(400);
		this.size = size;
		cloud = new Image("testdata/coffeerun_v2_images/cloudSimple - Kopie.png");

		if (this.size == size.small) {
			this.width = 60;
			this.height = 60;
			this.velocity = 0.40f;
		}

		if (this.size == size.medium) {
			this.width = 90;
			this.height = 90;
			this.velocity = 0.55f;
		}

		if (this.size == size.big) {
			this.width = 120;
			this.height = 120;
			this.velocity = 0.65f;
		}

	}

	public void update(int delta) {
		this.x -= delta * this.velocity;
		
		if (this.x < -100) {
			this.x = 2000;
		}
	}

	public void render(Graphics g) {

	

		cloud.draw(this.x, this.y, this.width, this.height);
	}

}
