package at.leon.games.cofferun.actors;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {

	public void render(Graphics g);

	void update(GameContainer arg0, int delta);
}
