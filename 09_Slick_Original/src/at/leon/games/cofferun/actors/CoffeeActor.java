package at.leon.games.cofferun.actors;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
//import at.leon.games.cofferun.actors.Cloud.size;

public class CoffeeActor implements Actor {
	
	private float x, y, width, height, speed;
	private float jumpStrength, weight;
	private Image coffee;
	private boolean jump;

	Random rnd = new Random();

	public CoffeeActor() throws SlickException{
		// TODO Auto-generated constructor stub
		super();
		coffee = new Image("testdata/coffeerun_v2_images/coffee.jpg");
	}

	@Override
	public void update(GameContainer arg0, int delta) {
		// TODO Auto-generated method stub
		jump = arg0.getInput().isKeyDown(Input.KEY_SPACE);
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		coffee.draw(x, this.y, this.width, this.height);
	}
}
