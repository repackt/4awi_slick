package at.leon.games.cofferun;

import java.util.List;
import java.util.ArrayList;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import at.leon.games.cofferun.actors.Cloud;
import at.leon.games.cofferun.actors.CoffeeActor;
import slick_introduction.Snowflake;

public class Main extends BasicGame {

	List<Cloud> clouds;
	
	public Main() {
		super("CoffeeRun_v2");
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		
		g.setBackground(Color.decode("#4286f4"));
		for (int i = 0; i < clouds.size(); i++) {
			clouds.get(i).render(g);
		}

	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		clouds = new ArrayList<Cloud>();
		
		for (int i = 0; i < 3; i++) {
			clouds.add(new Cloud(Cloud.size.small));
			clouds.add(new Cloud(Cloud.size.medium));
			clouds.add(new Cloud(Cloud.size.big));
		}
	}

	@Override
	public void update(GameContainer arg0, int delta) throws SlickException {
		for (int i = 0; i < clouds.size(); i++) {
			clouds.get(i).update(delta);
		}

	}

	public static void main(String[] args) {
		try {
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(1920, 1080, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
