package baseGame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class VanillaGame extends BasicGame {

	private double xR; // x-Rectangle
	private double yR; // y-Rectangle
	private double xC; // x-Circle
	private double yC; // y-Circle
	private double xO; // x-Oval
	private double yO; // y-Oval
	private double angle = 0;

	public int movementC = 0; // Circle
	public int movementO = 0; // Oval

	public VanillaGame() {
		super("FirstGame");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		// graphics.drawString("Hello", (int)this.x, 100);
		graphics.drawRect((int) xR, (int) yR, 100, 100);
		graphics.drawRoundRect((int) xC, (int) yC, 100, 100, 90);
		graphics.drawOval((int) xO, (int) yO, 100, 200);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		// Circle
		this.xC = 300;
		this.yC = 300;

		// Oval
		this.xO = 300;
		this.yO = 200;

	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		// Rectangle

		this.angle += delta * 0.3;
		this.xR = Math.cos(this.angle * Math.PI / 180) * 180 + 200;
		this.yR = Math.sin(this.angle * Math.PI / 180) * 180 + 300;

		// Circle
		if (movementC == 0) {
			this.xC += (double) delta * 0.2;
			if (xC >= 700) {
				movementC = 1;
			}
		}
		if (movementC == 1) {
			this.xC -= (double) delta * 0.2;
			if (xC <= 0) {
				movementC = 0;
			}
		}

		// Oval
		if (movementO == 0) {
			this.yO += (double) delta * 0.8;
			if (yO >= 400) {
				movementO = 1;
			}
		}
		if (movementO == 1) {
			this.yO -= (double) delta * 0.8;
			if (yO <= 0) {
				movementO = 0;
			}
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new VanillaGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
